import { createStore } from 'vuex'
const store = createStore({
    state: {
       productsAtCart: []
     },
     mutations: {
         setProductsAtCart: (state, productsAtCart) => state.productsAtCart = productsAtCart,
         addProductAtCart: (state, product) => {
            let sameElement = state.productsAtCart.find(element => element.product == product);
            
            if (sameElement)
                sameElement.count += 1;
            else
                state.productsAtCart.push({product: product, count: 1});
        },
     },
     getters: {
         getProductsAtCart: (state) =>
         {
             return state.productsAtCart
         }
     }  
 })
export default store;